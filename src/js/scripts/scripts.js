$('.js-trigger').on('click', function () {
  $(this).parent().next().toggle();
});
$('.dropdown__selected').on('click', function () {
  $(this).find('.dropdown__list').toggle();
  $(this).toggleClass('focus');
  $('.dropdown__selected').not(this).find('.dropdown__list').hide();
  $('.dropdown__selected').not(this).removeClass('focus');
});

$(document).mouseup(function (e) {
  var div = $(".dropdown__selected");
  if (!div.is(e.target) &&
    div.has(e.target).length === 0) {
    $('.dropdown__list').hide();
    $('.dropdown__selected').removeClass('focus');
  }
});

$('[data-fancybox]').fancybox({
  touch: {
    vertical: false,
    momentum: true
  },
});


$('.more-info').on('click', function (e) {
  e.preventDefault()
  $('.about-product ul li').slideDown();
  $(this).hide();
});



$('.delivery-region__input input[type="text"]').blur(function () {
  if ($(this).val()) {
    $(this).parent().find('label').addClass("focused");
  } else {
    $(this).parent().find('label').removeClass("focused");
  }
});


var hiddenList = $('.header__bottom-col .nav a').slice(5);
$('.nav__hidden-list').html(hiddenList);

$('.trigger-list').on('click', function () {
  $('.nav__hidden-list').toggle();
});


$(document).mouseup(function (e) {
  var div = $(".nav__hidden-list");
  if (!div.is(e.target) &&
    div.has(e.target).length === 0) {
    $('.nav__hidden-list').hide();
  }
});

$('.swiper-container, .nav__hidden-list a').on('click', function () {
  $('.nav__hidden-list').hide();
});
$(".nav__item:not('.nav__hidden-list .nav__item')").mouseover(function () {
  $('.nav__hidden-list').hide();
});

var t = 0;
$('.nav__item').hover(function () {
    var href = $(this).attr('data-href');
    $(href).show();
  },
  function () {
    var href = $(this).attr('data-href');
    if (!$(href).is(':hover') && !$(href).is(':focus'))
      $(href).hide()
  }
)
$('.submenu').hover(
  function () {
    clearTimeout(t);
  },
  function () {
    var t = setTimeout(() => {
      $('.nav__item').trigger('mouseout')
    }, 700)
  }
)

$('.footer__col .title:not(".title--without-arrow")').on('click', function () {
  $(this).next().slideToggle();
  $(this).toggleClass('--open')
  $('.footer__col .title').not(this).next('.footer__col-item').slideUp();
  $('.footer__col .title').not(this).next('.title').toggleClass('--open');
});




$('.dropdown-trigger').on('click', function () {
  $(this).toggleClass('--open');
  $(this).parent().parent().children('ul').slideToggle();
});


$('.filter__selected').on('click', function () {
  $(this).parent().find('.filter__dropdown').toggleClass('--open');
  $('.filter__selected').not(this).parent().find('.filter__dropdown').removeClass('--open');
});




jQuery(function ($) {
  $(document).mouseup(function (e) {
    var div = $(".filter__dropdown");
    if (!div.is(e.target) &&
      div.has(e.target).length === 0) {
      $('.filter__dropdown').removeClass('--open');
    }
  });
});


$('.respFilterItem').on('click', function () {
  var hrefFilter = $(this).attr('href');
  $(hrefFilter).addClass('--open');
  $('.filter-wraper-js').addClass('transform');
  $('body').addClass('no-scroll');
});

$('.mobile-sidebar-category').on('click', function () {
  $('.current-category').toggleClass('--open');
  $('.mobile-category-list').toggle();
});
$('.mobile-category-list a').on('click', function () {
  $('.mobile-category-list').hide();
  $('.current-category').removeClass('--open');
});
$(document).mouseup(function (e) {
  var div = $(".mobile-category-list, .mobile-sidebar-category");
  if (!div.is(e.target) &&
    div.has(e.target).length === 0) {
    $('.mobile-category-list').hide();
    $('.current-category').removeClass('--open');
  }
});

$('.filter__mobile-trigger').on('click', function () {
  $('body').addClass('no-scroll');
  $('.responsive-filter').toggleClass('--open');
  $('.responsive-filter').find('.filter-wraper-js').toggleClass('--open');
});

$('.close-filter').on('click', function () {
  $('body').removeClass('no-scroll');
  setTimeout(function () {
    $('.responsive-filter').removeClass('--open');
  }, 300);
  $('.filter-wraper-js').removeClass('--open');
});

$('.search-mobile').on('click', function () {
  $('.mobile-search').addClass('--open');
  $('body').addClass('no-scroll');
});

$('.hamburger').on('click', function () {
  $('.mobile-menu-wrap').addClass('--show');
  $('.mobile-menu').addClass('--open');
  $('body').addClass('no-scroll');
});

$('.mobile-menu-bg').on('click', function () {
  $('.mobile-menu').removeClass('--open');
  setTimeout(function () {
    $('.mobile-menu-wrap').removeClass('--show');
  }, 400);
  $('body').removeClass('no-scroll');
});
$('.mobile-menu-location').on('click', function () {
  $('body').addClass('no-scroll');
  $('.deliveryRegion-mobile').addClass('--show');
  $('.deliveryRegion-mobile .delivery-region').addClass('--open');
});


$('.trigger-js').on('click', function () {
  $(this).toggleClass('--open');
  $(this).parent().parent().children('ul').slideToggle();
});



$(document).mouseup(function (e) {
  var div = $(".responsive-filter__window.--open:not('.filter-wraper-js')");
  if (!div.is(e.target) &&
    div.has(e.target).length === 0) {
    $(".responsive-filter__window.--open:not('.filter-wraper-js')").removeClass('--open');
    $('.filter-wraper-js').removeClass('transform');
  }
});

$(document).mouseup(function (e) {
  var div = $(".delivery-region, .mobile-menu-location, .responsive-filter, .mobile-search .input");
  if (!div.is(e.target) &&
    div.has(e.target).length === 0) {
    $('body').removeClass('no-scroll');
    $('.delivery-region').removeClass('--open');
    $('.mobile-search').removeClass('--open');
    setTimeout(function () {
      $('.deliveryRegion-mobile').removeClass('--show');
    }, 500);
  }
});
