var swiper = new Swiper(".tile__slider", {
  pagination: {
    el: ".swiper-pagination",
    type: "fraction",
  },
  navigation: {
    nextEl: ".tile__slide-next",
    prevEl: ".tile__slide-prev",
  },
});

var swiper = new Swiper(".category-slider", {
  slidesPerView: 11,
  speed: 10000,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false,
  },
  breakpoints: {
    320: {
      slidesPerView: 3
    },
    480: {
      slidesPerView: 5
    },
    640: {
      slidesPerView: 6
    },
    767: {
      slidesPerView: 7
    },
    992: {
      slidesPerView: 8
    },
    1200: {
      slidesPerView: 11
    }
  }
});

var swiper = new Swiper(".card-popular", {
  slidesPerView: 6,
  spaceBetween: 20,
  slidesPerGroup: 6,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    320: {
      slidesPerView: 2
    },
    480: {
      slidesPerView: 3
    },
    640: {
      slidesPerView: 4
    },
    992: {
      slidesPerView: 5
    },
    1200: {
      slidesPerView: 6
    }
  }
});

var swiper = new Swiper(".card-slider", {
  slidesPerView: 6,
  spaceBetween: 10,
  slidesPerGroup: 6,
  navigation: {
    nextEl: ".card-next",
    prevEl: ".card-prev",
  },
  breakpoints: {
    320: {
      slidesPerView: 2
    },
    480: {
      slidesPerView: 3
    },
    640: {
      slidesPerView: 4  
    },
    992: {
      slidesPerView: 5
    },
    1200: {
      slidesPerView: 6
    }
  }
});



var swiper = new Swiper(".banner-slider", {
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  }
});

$('.card__item-img').on('click', function () {
  var swiper = new Swiper(".popup-card-slider", {
    loop: true,
    slidesPerView: 1,
    allowTouchMove: false,
    navigation: {
      nextEl: ".popup-card__prev",
      prevEl: ".popup-card__next",
    },
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
    },
  });
});

var swiper = new Swiper(".product__slider", {
  loop: true,
  slidesPerView: 2,
  allowTouchMove: false,
  navigation: {
    nextEl: ".prev",
    prevEl: ".next",
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  }
});